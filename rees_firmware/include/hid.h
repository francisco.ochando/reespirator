#define SCREEN_HOME       0
#define SCREEN_CONFIG     1
#define SCREEN_VIEW       2
#define SCREEN_START      3
#define SCREEN_EDIT       4
#define SCREEN_REV        5
#define SCREEN_ALARMAS    5
#define SCREEN_FREC      11
#define SCREEN_PEAK      12 
#define SCREEN_PEEP      13
#define SCREEN_VOL       14

#define SWITCH_ENC        1		// Por asignar
#define SWITCH_PEAK       2		// Por asignar
#define SWITCH_PEEP       3		// Por asignar
#define SWITCH_ALARMAS    4		// Por asignar


enum State {
    SCREEN_REPOSO = 2,
    SCREEN_CONFIG = 3,  
    SCREEN_REPOSO_CONFIG = 4,
    SCREEN_START = 5,
    SCREEN_REVISION_CONSIGNA = 7,
    SCREEN_CONSIGNAS = 8,
    SCREEN_EDIT_TRIGGER = 9,
    SCREEN_RECRUIT = 10,
    SCREEN_EDIT_RECRUIT = 11,
    SCREEN_CONFIRMA_REPOSO = 12,
    SCREEN_CONFIRMA_OFF = 13
};


class HID
{
    
    // Clase: hid.cpp
     
    // Contiene la clase que maneja el interface humano y la máquina de estados
    // Recibe las entradas de los botones y envía las llamadas a la pantalla.
    
    // Contiene los siguientes metodos de la clase
    
    private:
        byte hidestadoanterior;
        byte hidestadoactual;

        // Metodos
        byte HID::Update( byte estado )
        byte HID::read( byte entrada, byte estado )

    public:
 	
        // Metodos
        bool readButton( byte boton );
        bool readAlarm();
        void readAlarm( byte codigo );
        byte readHID( void );
        byte getEstado( void );
        byte Update( byte estado );
        byte HID::getInput( void );
        void writeValues( void );
        void writeAlarms( void );
		
		
}