/*
     Clase: hid.cpp
     
     Contiene la clase que maneja el interface humano y la máquina de estados
     Recibe las entradas de los botones y envía las llamadas a la pantalla.
     
     Contiene los siguientes metodos de la clase

     Private:
        byte HID::Change( void )
        byte HID::read( void )
        
     Public:
        byte HID::getEstado( void )
        byte HID::setEstado( void )
        byte HID::getInputs( void )
      
 */

#include "Reespirator.h"

extern Reespirator miRespirator;




byte HID::Change( byte inp, byte estado )
{
    byte miestado = estado;
    //    Rotary Switch
    if( ( inp == SWITCH_01 || inp == SWITCH_02 ) && hidestadoanterior == SCREEN_HOME )
    {
        // Go to Config screen
        miRespirator.pantalla.Screen(1, 0);
        hidestadoactual = 1;
    }
    else if( ( inp == SWITCH_03 || inp == SWITCH_ENC ) && hidestadoanterior == SCREEN_HOME )
    {
        // Go to Standby screen
        miRespirator.pantalla.MainScreen(2, DEFAULT_RPM);
        hidestadoactual = 2;
    }
    if( ( inp == SWITCH_01 ) && hidestadoanterior == SCREEN_CONFIG )
    {
        // Go to Standby screen
        miRespirator.pantalla.Screen(2, 0);
        hidestadoactual = 2;
    }
    if( ( inp == SWITCH_01 ) && hidestadoanterior == SCREEN_VIEW )
    {
        // Go to Config screen
        miRespirator.pantalla.Screen(1, 0);
        hidestadoactual = 1;
    }
    if( ( inp == SWITCH_02 ) && hidestadoanterior == SCREEN_VIEW )
    {
        // Go to Started screen
        miRespirator.pantalla.Screen(3, 0);
        hidestadoactual = 3;
    }
    else if( boton == SWITCH_ENC && hidestadoanterior == SCREEN_VIEW )
    {
        // Go to Stopping screen
        miRespirator.pantalla.Screen(99, 0);
    	hidestadoactual = 99;
    }
    else if( boton == SWITCH_ENC && hidestadoanterior == SCREEN_MENU )
    {
        // Go Submenu
        miRespirator.pantalla.Screen(1, DEFAULT_RPM);
        hidestadoactual = 1;
    }
    else if( boton == SWITCH_2 && hidestadoanterior == SCREEN_HOME )
    {
        // Go Menu
        miRespirator.pantalla.Screen(2, DEFAULT_VOL);
        hidestadoactual = 2;    
    }
    else if( boton == SWITCH_3 && hidestadoanterior == SCREEN_VIEW )
    {
        // Go Screen var PEAK
        miRespirator.pantalla.Screen(3, DEFAULT_PEAK);
        hidestadoactual = 3;    
    }
    else if( boton == SWITCH_4 && hidestadoanterior == SCREEN_VIEW )
  	{
        // Go Screen var PEEP
        miRespirator.pantalla.Screen(0, DEFAULT_PEEP);
        hidestadoactual = 4;    
    }
    else
    {
        hidestadoactual = estado; // El estado no varia
    }
    return miestado;     
}


/*

     Metodo:   read

     Private: Me devuelve el último botón pulsado en la máquina (byte)

 */

byte HID::read( void )
{
  	byte pulsado = 0;
  	//    Rotary Switch (01)
  	if( miRespirator.encoder.readButton() ) // Enlaza con código de produccion 
  	{
      	pulsado = 1;
  	}
  	else if( this.readButton(SWITCH_PICO) )
	{
  		//    Switch  02
    		pulsado = 2;
  	}
  	else if( this.readButton(SWITCH_PEEP) )
	{
  		//    Switch  03
    		pulsado = 3;
  	}
  	else if ( this.readButton(SWITCH_ALARMAS) )
  	{
  		//    Switch  04
    		pulsado = 4;
  	}
  	else
  	{
    		pulsado = 0;
  	}
  	return pulsado;
}

    
/*

     Metodo:   getEstado

     Public: Me devuelve el estado actual de la máquina (byte)

 */
 
byte HID::getEstado( void )
{
  	return miRespirator.HID.hidestadoactual;   
}


/*

     Metodo:   setEstado

     Public: Me cambia el estado de la máquina. Devuelve el estado actual (byte)

 */
    
byte HID::setEstado( void )
{
  	miestado = miRespirator.HID.CambiaEstado( byte boton, estado);
  	return miestado;  
}


/*

     Metodo:   getInput

     Public: Me devuelve el estado de la entrada a la máquina (byte)
             Una pulsación simultánea de dos botones debería dar prioridad a la última.

 */

byte HID::getInputs( void )
{
	miboton = miRespirator.HID.Lee();
  	return miboton;
} 
